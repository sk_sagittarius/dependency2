﻿using Container.Data;
using Datalib.Models;
using Datalib.Models.Interfaces.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Container.Controllers
{
    public class HomeController : Controller
    {
        private readonly CarRepository repo = new CarRepository();
        public ActionResult Index()
        {
            var cars = repo.Get(new PirelliTyre());
            return View(cars);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}