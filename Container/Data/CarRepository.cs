﻿using Datalib.Models;
using Datalib.Models.Interfaces.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Container.Data
{
    public class CarRepository
    {
        private IList<Car> Cars; // repository

        public IList<Car> Get(ITyre tyre)
        {
            Cars = new List<Car>()
            {
                new Car(1, "Mers", "black", tyre),
                new Car(2, "BMW", "blue", tyre),
                new Car(3, "KIA", "white", tyre)
            };
            return Cars;
        }
    }
}