﻿using Datalib.Models;
using Datalib.Models.Interfaces.Implementation;
using Ninject.Modules;

namespace Injector
{
    public class NinjectContainer : NinjectModule
    {
        public override void Load()
        {
            Bind<ITyre>().To<PirelliTyre>();
        }
    }

}