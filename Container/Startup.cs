﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Container.Startup))]
namespace Container
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
