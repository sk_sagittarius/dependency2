﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datalib.Models
{
    public class Car
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Color { get; set; }
        public ITyre Tyre { get; set; }

        public Car() { }
        public Car(int id, string brand, string color, ITyre tyre)
        {
            Id = id;
            Brand = brand;
            Color = color;
            Tyre = tyre;
        }
    }

    //public class Brand
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}
