﻿namespace Datalib.Models
{
    public interface IEngine
    {
        int Id { get; set; }
        double Power { get; set; }
        string Name { get; set; }

        void SetPower();
        void SetName();
    }
}
