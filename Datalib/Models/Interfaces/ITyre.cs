﻿namespace Datalib.Models
{
    public interface ITyre
    {
        int Id { get; set; }
        string Brand { get; set; }
    }
}
