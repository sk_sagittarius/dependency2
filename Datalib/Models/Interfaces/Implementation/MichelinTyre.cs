﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datalib.Models.Interfaces.Implementation
{
    public class MichelinTyre : ITyre
    {
        public int Id { get; set; }
        public string Brand { get; set; }

        public MichelinTyre()
        {
            Brand = "Michelin";
        }
    }
}
